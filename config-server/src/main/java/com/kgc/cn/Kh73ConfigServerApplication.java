package com.kgc.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class Kh73ConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Kh73ConfigServerApplication.class, args);
    }

}
